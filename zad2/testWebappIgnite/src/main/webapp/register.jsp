<%--
  Created by IntelliJ IDEA.
  User: derlecki
  Date: 02.02.2021
  Time: 14:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

<%
    if (!(session == null || session.getAttribute("uid") == null || session.getAttribute("uid") == "")) {
        response.sendRedirect("welcome.jsp"); // No logged-in user found, so redirect to login page.
    }
%>

<div style="display: flex; align-items: center; flex-direction: column; margin: auto; width: 60%; min-height: 100%; padding: 20px">
    <p class="h1 text-center mb-4">Register an account</p>
    <div style="
        padding-top: 30px;
        -webkit-border-radius: 10px 10px 10px 10px;
        border-radius: 10px 10px 10px 10px;
        background: #fff;
        width: 90%;
        max-width: 450px;
        position: relative;
        -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        text-align: center;"
    >
        <form action="Register" method="post" style="margin: 5px">
            <input class="form-control my-2" type="text" placeholder="Login" name="login" required
                   minlength="2" maxlength="50"/>
            <input class="form-control my-2" type="password" name="password" placeholder="Password" required
                   minlength="6" maxlength="20"/>
            <input class="form-control my-2" type="email" name="email" placeholder="Email" required
                   minlength="2" maxlength="50"/>
            <input class="form-control my-2" type="text" name="name" placeholder="First Name" required
                   minlength="2" maxlength="25"/>
            <input class="form-control my-2" type="text" name="lastname" placeholder="Last Name" required
                   minlength="2" maxlength="25"/>

            <input class="btn btn-primary my-2" type="submit" value="Register">
        </form>
        <div style="
            background-color: #f6f6f6;
            border-top: 1px solid #dce8f1;
            padding: 25px;
            text-align: center;
            -webkit-border-radius: 0 0 10px 10px;
            border-radius: 0 0 10px 10px;"
        >
            <span>Already have an account? <a href="/">Login</a></span>
        </div>
    </div>
</div>

</div>

</body>
</html>
