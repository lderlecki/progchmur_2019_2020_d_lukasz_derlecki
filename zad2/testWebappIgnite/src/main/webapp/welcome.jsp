<%--
  Created by IntelliJ IDEA.
  User: derlecki
  Date: 02.02.2021
  Time: 16:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

<%
    if (session == null || session.getAttribute("uid") == null || session.getAttribute("uid") == "") {
        response.sendRedirect("index.jsp"); // No logged-in user found, so redirect to login page.
    }
%>
    <div class="container">
        <h1 class="mt-5">User logged in!</h1>
        <p class="lead">If you want to logout, just press the button below.</p>
        <form action="Logout" method="post">
            <input class="btn btn-danger" type="submit" value="Logout">
        </form>
    </div>
</body>
</html>
