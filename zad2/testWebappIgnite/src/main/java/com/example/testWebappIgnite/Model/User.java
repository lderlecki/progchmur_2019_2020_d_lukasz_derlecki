package com.example.testWebappIgnite.Model;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String uname;
    private String name;
    private String lastname;
    private String email;
    private String pass;

    public User() {
        this.uname = "";
        this.name = "";
        this.lastname = "";
        this.email = "";
        this.pass = "";
    }

    public User(String uname, String name, String lastname, String email, String pass) {
        this.uname = uname;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.pass = pass;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}