package com.example.testWebappIgnite.db;

import com.example.testWebappIgnite.Model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class userDb {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public int createUser(User u) throws SQLException {
        int result = 0;
        String query = "INSERT INTO users(login, password, email, firstname, lastname) VALUES(?, ?, ?, ?, ?)";
        con = Connect.dbCon();

        ps = con.prepareStatement(query);
        ps.setString(1, u.getUname());
        ps.setString(2, u.getPass());
        ps.setString(3, u.getEmail());
        ps.setString(4, u.getName());
        ps.setString(5, u.getLastname());

        result = ps.executeUpdate();

        con.close();
        return result;
    }

    public boolean checkUserExists(User u) throws SQLException {
        String query = "SELECT * FROM users WHERE login=? AND email=?";
        con = Connect.dbCon();
        ps = con.prepareStatement(query);
        ps.setString(1, u.getUname());
        ps.setString(2, u.getPass());

        rs = ps.executeQuery();
        if (!rs.next())
            return true;
        return false;
    }


}
