package com.example.testWebappIgnite.controller;

import com.example.testWebappIgnite.Model.User;
import com.example.testWebappIgnite.db.userDb;
import sun.misc.Request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "Register", value = "/Register")
public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        if user logged in redirect to index
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        userDb db = new userDb();
        int res = 0;
        user.setUname(request.getParameter("login"));
        user.setPass(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setName(request.getParameter("name"));
        user.setLastname(request.getParameter("lastname"));

        try {
            if (db.checkUserExists(user)) {
                res = db.createUser(user);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        }

        if (res != 0) {
            RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            rd.forward(request, response);
        }else {
            response.sendRedirect("register.jsp");
        }

    }
}
