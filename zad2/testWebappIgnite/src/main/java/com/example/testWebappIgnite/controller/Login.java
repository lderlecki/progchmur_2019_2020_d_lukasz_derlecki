package com.example.testWebappIgnite.controller;

import com.example.testWebappIgnite.Model.User;
import com.example.testWebappIgnite.db.userDb;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "Login", value = "/Login")
public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (!((session.getAttribute("uid") == null) || (session.getAttribute("uid") == ""))){
            response.sendRedirect("index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        userDb db = new userDb();


        HttpSession session = request.getSession();
        user.setUname(request.getParameter("login"));
        user.setPass(request.getParameter("password"));

        System.out.println("login: " + user.getUname());
        System.out.println("password: " + user.getPass());

        try {
            if (db.checkUserExists(user)) {
                session.setAttribute("uid", user.getUname());
                response.sendRedirect("index.jsp");
            } else {
                response.sendRedirect("login.jsp");
                session.setAttribute("uid", "");
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }

    }
}
