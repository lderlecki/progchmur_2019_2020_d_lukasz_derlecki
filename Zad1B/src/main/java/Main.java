import com.google.gson.Gson;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DataSerializer {
    // class used to serializer json to object
    Integer series;
    String side;
    Features2D features2D;

    public class Features2D {
        // serializer features2d from json to object
        double fifth;
        double fourth;
        double first;
        double third;
        double second;

        public Map<String, Double> serializeJsonObj() {
            // map json values to variables in object
            Map<String, Double> hashMap = new HashMap<>();
            hashMap.put("first", first);
            hashMap.put("second", second);
            hashMap.put("third", third);
            hashMap.put("fourth", fourth);
            hashMap.put("fifth", fifth);
            return hashMap;
        }
    }
}

public class Main {
    public static double calculateStd(List<Double> arr) {
        double sum = 0;
        double std = 0;
        double arrLength = arr.size();

        for (double number : arr) sum += number;
        for (double number : arr) std += Math.pow(number - (sum / arrLength), 2);

        return Math.sqrt(std / arrLength);
    }

    public static class FingerMapper extends Mapper<Object, Text, Text, DoubleWritable> {
        public void map(Object key, Text value, Context context)
                throws IOException, InterruptedException {
            Gson gson = new Gson();

            // map json to obj of DataSerializer class
            DataSerializer data = gson.fromJson(String.valueOf(value), DataSerializer.class);
            // create log prefix for output file
            String logPrefix = String.format("%s-%s-", data.side, data.series);
            Map<String, Double> dataValuesMap = data.features2D.serializeJsonObj();

            for (String dataKey : dataValuesMap.keySet())
                context.write(new Text(logPrefix + dataKey), new DoubleWritable(dataValuesMap.get(dataKey)));

        }
    }

    public static class FingerReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        public void reduce(Text key, Iterable<DoubleWritable> values, Context context)
                throws IOException, InterruptedException {
            List<Double> dataValues = new ArrayList<>();
            for (DoubleWritable value : values) dataValues.add(value.get());
            double std = calculateStd(dataValues);
            context.write(key, new DoubleWritable(std));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "std for fingers data");
        job.setJarByClass(Main.class);

        job.setMapperClass(Main.FingerMapper.class);
        job.setReducerClass(Main.FingerReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }
}